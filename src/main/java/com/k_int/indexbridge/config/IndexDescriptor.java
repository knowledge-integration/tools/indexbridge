/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties
public class IndexDescriptor {

	private String indexName = "default";
	private String clusterName = "elasticsearch";
	private String host = "localhost";
	private String protocol = "http";
	private String username;
	private String password;
	private String indexType = "elasticsearch:8";
	private int port = 9200;
	private String indexConfigurationPath;
	private String batchTokenPath = "id";


	public String getSummary() {
		return clusterName + "/" + indexName + "@" + host + ":" + port;
	}
}
