/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.interceptor.RetryInterceptorBuilder;
import org.springframework.retry.interceptor.RetryOperationsInterceptor;

@Data
@Configuration
@ConfigurationProperties(prefix = "retry")
public class RetryConfig {

	private int maxAttempts = 5;
	private int initialInterval = 3000;
	private int maxInterval = 60000;
	private double multiplier = 1.5;

	@Bean
	public RetryOperationsInterceptor statelessRetryInterceptor() {
		return RetryInterceptorBuilder.stateless()
				.maxAttempts(maxAttempts)
				.backOffOptions(initialInterval, multiplier, maxInterval)
				.build();
	}
}
