/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge;

import com.k_int.indexbridge.migration.MigrationManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class IndexBridge implements CommandLineRunner {

	@Autowired
	private MigrationManager migrationManager;

	public static void main(String[] args) {
		SpringApplication.run(IndexBridge.class, args);
	}

	@Override
	public void run(String... args) {
		migrationManager.run();
	}
}
