/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.persistence;

import com.k_int.indexbridge.error.PersistenceException;
import com.k_int.indexbridge.migration.IndexBatch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.*;

@Slf4j
@Component
class BatchPersistenceManagerImpl implements BatchPersistenceManager {

	private static final String DATA_DIR = "data";

	public BatchPersistenceManagerImpl() {
		File batchDir = new File(DATA_DIR);

		boolean created = batchDir.mkdir();
		if (created)
			log.trace("Created data directory at {}", batchDir.getPath());
	}

	@Override
	public void write(IndexBatch batch) {
		File batchFile = getBatchFile(batch.getBatchDescriptor().getId());

		if (batchFile.exists()) {
			log.debug("Batch file {} already exists", batchFile.getPath());
		}

		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(batchFile))) {
			oos.writeObject(batch);
			log.trace("Wrote batch {}", batchFile.getPath());
		} catch (IOException e) {
			throw new PersistenceException("Failed to write batch", e);
		}
	}

	@Override
	public IndexBatch read(String id) {
		File batchFile = getBatchFile(id);

		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(batchFile))) {
			return (IndexBatch) ois.readObject();
		} catch (EOFException e) {
			// If we have a corrupted batch file it was probably the last
			// batch to be retrieved, so we can skip it since hopefully
			// the previous persisted batch is intact
			log.warn("Batch {} may be corrupted", batchFile.getPath());
			delete(id);
			return new IndexBatch();
		}
		catch (IOException | ClassNotFoundException e) {
			throw new PersistenceException("Failed to read batch", e);
		}
	}

	@Override
	public void delete(String id) {
		File batchFile = getBatchFile(id);

		if (batchFile.delete()) {
			log.trace("Deleted batch {}", batchFile.getPath());
		} else {
			log.warn("Batch file {} does not exist", batchFile.getPath());
		}
	}

	@Override
	public String[] getPersistedBatches() {
		File batchDir = new File(DATA_DIR);

		return batchDir.list();
	}

	private File getBatchFile(String id) {
		return new File(DATA_DIR + File.separator + id);
	}
}
