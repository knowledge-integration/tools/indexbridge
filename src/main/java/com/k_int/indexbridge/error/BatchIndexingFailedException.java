/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.error;

import com.k_int.indexbridge.client.TargetClient;
import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.migration.IndexBatch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchIndexingFailedException extends MigrationException {

	private IndexBatch batch;
	private IndexDescriptor indexDescriptor;
	private TargetClient client;

	public BatchIndexingFailedException(IndexBatch batch, IndexDescriptor indexDescriptor, TargetClient client, Throwable cause) {
		super(cause);
		this.batch = batch;
		this.indexDescriptor = indexDescriptor;
		this.client = client;
	}
}
