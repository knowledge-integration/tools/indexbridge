/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.error;

import com.k_int.indexbridge.config.IndexDescriptor;

import java.net.http.HttpResponse;

public class IndexRequestException extends MigrationException {
	public IndexRequestException(IndexDescriptor indexDescriptor, Throwable cause) {
		super("Request against index failed", indexDescriptor, cause);
	}

	public IndexRequestException(String message, IndexDescriptor indexDescriptor, Throwable cause) {
		super("Request against index failed: " + message, indexDescriptor, cause);
	}

	public IndexRequestException(IndexDescriptor indexDescriptor, HttpResponse<String> cause) {
		super("Request against index failed: " + cause.body(), indexDescriptor);
	}

	public <T> IndexRequestException(String message, HttpResponse<T> cause) {
		super(message + " " + cause.body());
	}

	public IndexRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
