/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.migration;

import com.k_int.indexbridge.client.*;
import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.config.MigrationConfig;
import com.k_int.indexbridge.error.MigrationFailureException;
import com.k_int.indexbridge.error.NoClientFoundException;
import com.k_int.indexbridge.processing.IndexingQueueService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
@Component
class MigrationManagerImpl implements MigrationManager {

	private final MigrationConfig migrationConfig;
	private final List<Client> clients;
	private final IndexingQueueService indexingQueueService;

	MigrationManagerImpl(@Autowired MigrationConfig config,
						 @Autowired List<Client> clients,
						 @Autowired IndexingQueueService indexingQueueService) {
		this.migrationConfig = config;
		this.clients = clients;
		this.indexingQueueService = indexingQueueService;

		log.debug("Found {} client(s)", clients.size());
	}

	@Override
	public void run() {
		try {
			migrate(migrationConfig.getSourceIndexDescriptor(), migrationConfig.getTargetIndexDescriptor());
		} catch (MigrationFailureException ex) {
			log.error("Migration failed", ex);
			System.exit(1);
		}
		System.exit(0);
	}

	private void migrate(IndexDescriptor sourceIndexDescriptor,
						 IndexDescriptor targetIndexDescriptor) throws MigrationFailureException {
		log.info("Preparing index migration...");
		log.debug("Finding applicable clients for index descriptors...");
		SourceClient sourceClient = (SourceClient) clients.stream().filter(c -> c.supportsIndex(sourceIndexDescriptor.getIndexType()))
				.findFirst().orElseThrow(() -> new NoClientFoundException(sourceIndexDescriptor.getIndexType()));

		TargetClient targetClient = (TargetClient) clients.stream().filter(c -> c.supportsIndex(targetIndexDescriptor.getIndexType()))
				.findFirst().orElseThrow(() -> new NoClientFoundException(targetIndexDescriptor.getIndexType()));

		log.info("Pinging source cluster {}...", sourceIndexDescriptor.getSummary());
		sourceClient.build(sourceIndexDescriptor);
		sourceClient.ping(sourceIndexDescriptor);

		log.info("Pinging target cluster {}...", targetIndexDescriptor.getSummary());
		targetClient.build(targetIndexDescriptor);
		targetClient.ping(targetIndexDescriptor);

		log.info("Checking source index {} exists...", sourceIndexDescriptor.getSummary());
		if (!sourceClient.exists(sourceIndexDescriptor)) {
			throw new MigrationFailureException(MessageFormat.format("Source index {0} doesn't exist", sourceIndexDescriptor.getSummary()));
		}

		long sourceIndexCount = sourceClient.documentCount(sourceIndexDescriptor);
		log.info("Source index {} contains {} documents to be migrated", sourceIndexDescriptor.getSummary(), sourceIndexCount);

		if (!targetClient.exists(targetIndexDescriptor)) {
			log.info("Index {} does not exist, creating...", targetIndexDescriptor.getSummary());
			targetClient.createIndex(targetIndexDescriptor);
		}

		BatchDescriptor batchDescriptor = new BatchDescriptor();
		batchDescriptor.setBatchTokenPath(sourceIndexDescriptor.getBatchTokenPath());
		batchDescriptor.setBatchSize(migrationConfig.getBatchSize());

		// Ask the indexing queue to re-queue any batches that haven't been indexed, and wait for them
		// to complete before we try to get a resumption point
		try {
			log.info("Checking for incomplete batches...");
			indexingQueueService.requeueBatches(targetIndexDescriptor, targetClient)
					.get(migrationConfig.getQueueClearedTimeout(), TimeUnit.MINUTES);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			throw new MigrationFailureException("Failed to re-queue batches", e);
		}

		// Get the max value of the batch token path from each index and compare. This assumes
		// that no batches below this value have failed to index into the target index. If this happens,
		// the migration can be restarted from scratch by disabling migration.allowResume
		Object sourceMaxBatchToken = sourceClient.max(sourceIndexDescriptor.getBatchTokenPath(), sourceIndexDescriptor);
		Object targetMaxBatchToken = targetClient.max(targetIndexDescriptor.getBatchTokenPath(), targetIndexDescriptor);
		log.debug("Source max batch token {}, target max batch token {}", sourceMaxBatchToken, targetMaxBatchToken);
		if (targetMaxBatchToken != null &&
				!Objects.equals(sourceMaxBatchToken, targetMaxBatchToken) &&
				migrationConfig.isAllowResume()) {
			log.info("Migration will resume from {}", targetMaxBatchToken);
			batchDescriptor.setBatchToken(String.valueOf(targetMaxBatchToken));
		} else {
			batchDescriptor.setBatchToken(migrationConfig.getBatchTokenInitialValue());
		}

		log.info("Starting migration from {} to {}", sourceIndexDescriptor.getSummary(), targetIndexDescriptor.getSummary());
		Instant start = Instant.now();

		List<IndexItem> sourceBatch;

		// Retrieve each batch from the source index synchronously, then queue them with the index queue service
		// to index them asynchronously into the target index
		do {
			sourceBatch = sourceClient.retrieveBatch(sourceIndexDescriptor, batchDescriptor);
			log.debug("Retrieved batch: token {}, remaining {}", batchDescriptor.getBatchToken(), batchDescriptor.getRemaining());
			List<IndexRequest> targetBatch = new ArrayList<>();

			for (IndexItem item : sourceBatch) {
				IndexRequest indexRequest = new IndexRequest();
				indexRequest.setId(item.getId());
				indexRequest.setSource(item.getSource());
				indexRequest.setIndex(targetIndexDescriptor.getIndexName());
				targetBatch.add(indexRequest);
			}

			IndexBatch batch = new IndexBatch();
			// We have to deep-copy the batch descriptor as it will be used
			// by multiple threads
			batch.setBatchDescriptor(new BatchDescriptor(batchDescriptor));
			batch.setIndexRequests(targetBatch);

			// Processing a batch involves writing it to the filesystem, indexing it, then removing
			// it from the filesystem. These three operations are captured by the future that gets
			// queued by the indexing queue service. If the queue is full, this call
			// will block until the next batch has completed
			indexingQueueService.queueBatch(indexingQueueService.indexBatch(batch, targetIndexDescriptor, targetClient));
		} while (sourceBatch.size() > 0);

		try {
			// Block until all remaining batches have completed or a timeout occurs
			log.info("Waiting for batch processing to complete...");
			indexingQueueService.queueCleared()
					.get(migrationConfig.getQueueClearedTimeout(), TimeUnit.MINUTES);
		} catch (InterruptedException | TimeoutException | ExecutionException e) {
			throw new MigrationFailureException("Some batches failed to complete", targetIndexDescriptor, e);
		}

		// At this point there should never be any remaining batches
		if (indexingQueueService.queueSize() != 0) {
			throw new MigrationFailureException("Queue is not empty after completion");
		}

		Instant end = Instant.now();
		log.info("Finished index migration");
		log.info("Migration completed in {} minutes", Duration.between(start, end).toMinutes());

		long targetIndexCount = targetClient.documentCount(targetIndexDescriptor);

		log.info("After batch completion {} has {} documents ({} has {}).",
				targetIndexDescriptor.getSummary(),
				targetIndexCount,
				sourceIndexDescriptor.getSummary(),
				sourceIndexCount);

		if (targetIndexCount != sourceIndexCount) {
			log.warn("Index counts do not match. The target index may still be persisting documents or some documents may have failed to be migrated due to indexing errors.");
		}
	}
}
