/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.migration;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class BatchDescriptor implements Serializable {
	private int batchSize;
	private String batchToken;
	private String batchTokenPath;
	private int remaining;


	public BatchDescriptor(BatchDescriptor batchDescriptor) {
		this.batchSize = batchDescriptor.getBatchSize();
		this.batchToken = batchDescriptor.getBatchToken();
		this.batchTokenPath = batchDescriptor.getBatchTokenPath();
		this.remaining = batchDescriptor.getRemaining();
	}

	public String getId() {
		return batchToken;
	}
}
