/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.client;

import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.error.IndexRequestException;
import org.springframework.retry.annotation.Retryable;

import java.io.IOException;
import java.net.SocketTimeoutException;

public interface Client {

	/**
	 * Check that the cluster containing the source/target index is available
	 *
	 * @param indexDescriptor the index to ping
	 * @throws IndexRequestException in the case of the index being unavailable
	 */
	@Retryable(value = {IOException.class, SocketTimeoutException.class}, interceptor = "statelessRetryInterceptor")
	void ping(IndexDescriptor indexDescriptor) throws IndexRequestException;

	/**
	 * Check that the source/target index exists
	 *
	 * @param indexDescriptor the index to check exists
	 * @return whether the index exists
	 */
	@Retryable(value = {IOException.class, SocketTimeoutException.class}, interceptor = "statelessRetryInterceptor")
	boolean exists(IndexDescriptor indexDescriptor) throws IndexRequestException;

	/**
	 * Count the number of documents in an index
	 *
	 * @param indexDescriptor the index to count documents within
	 * @return the number of documents matched by the index descriptor
	 */
	@Retryable(value = {IOException.class, SocketTimeoutException.class}, interceptor = "statelessRetryInterceptor")
	int documentCount(IndexDescriptor indexDescriptor);

	/**
	 * Get the max value of a field. Used for getting a point to resume from
	 */
	Object max(String field, IndexDescriptor indexDescriptor);

	void build(IndexDescriptor indexDescriptor);

	default boolean supportsIndex(String indexVersion) {
		return true;
	}
}
