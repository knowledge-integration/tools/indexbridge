/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.client.es5;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.k_int.indexbridge.client.*;
import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.error.ConfigurationException;
import com.k_int.indexbridge.error.IndexRequestException;
import com.k_int.indexbridge.migration.BatchDescriptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
class Es5Client implements SourceClient {

	private HttpClient httpClient;
	private ObjectMapper objectMapper;

	@Override
	public void build(IndexDescriptor indexDescriptor) {
		this.httpClient = HttpClient.newHttpClient();
		this.objectMapper = new ObjectMapper();
	}

	@Override
	public int documentCount(IndexDescriptor indexDescriptor) {
		try {
			HttpRequest countRequest = HttpRequest.newBuilder()
					.GET()
					.uri(new URI(indexDescriptor.getProtocol(),
							null,
							indexDescriptor.getHost(),
							indexDescriptor.getPort(),
							"/" + indexDescriptor.getIndexName() + "/_count",
							null,
							null))
					.build();

			HttpResponse<String> response = performRequest(countRequest, HttpResponse.BodyHandlers.ofString());

			Map<String, Object> responseAsMap = objectMapper.readValue(response.body(), new TypeReference<>() {
			});

			return (int) responseAsMap.get("count");
		} catch (URISyntaxException e) {
			throw new ConfigurationException("Request URI not properly formed", indexDescriptor, e);
		} catch (JsonProcessingException e) {
			throw new IndexRequestException("Failed to create map from response", indexDescriptor, e);
		}
	}

	@Override
	public void ping(IndexDescriptor indexDescriptor) throws IndexRequestException {
		try {
			HttpRequest pingRequest = HttpRequest.newBuilder()
					.GET()
					.uri(new URI(indexDescriptor.getProtocol(),
							null,
							indexDescriptor.getHost(),
							indexDescriptor.getPort(),
							null,
							null,
							null))
					.build();

			HttpResponse<String> response = performRequest(pingRequest, HttpResponse.BodyHandlers.ofString());

			if (response.statusCode() != 200) {
				throw new IndexRequestException(indexDescriptor, response);
			}
		} catch (URISyntaxException e) {
			throw new ConfigurationException("Request URI not properly formed", indexDescriptor, e);
		}
	}

	@Override
	public boolean exists(IndexDescriptor indexDescriptor) throws IndexRequestException {
		try {
			HttpRequest pingRequest = HttpRequest.newBuilder()
					.GET()
					.uri(new URI(indexDescriptor.getProtocol(),
							null,
							indexDescriptor.getHost(),
							indexDescriptor.getPort(),
							"/" + indexDescriptor.getIndexName(),
							null,
							null))
					.build();

			HttpResponse<String> response = performRequest(pingRequest, HttpResponse.BodyHandlers.ofString());

			return response.statusCode() == 200;
		} catch (URISyntaxException e) {
			log.error("Invalid URI for {}", indexDescriptor.getSummary(), e);
			return false;
		}

	}

	@Override
	public List<IndexItem> retrieveBatch(IndexDescriptor indexDescriptor, BatchDescriptor batchDescriptor) throws IndexRequestException {
		try {
			URI uri = new URIBuilder(new URI(indexDescriptor.getProtocol(),
					null,
					indexDescriptor.getHost(),
					indexDescriptor.getPort(),
					"/" + indexDescriptor.getIndexName() + "/_search",
					null,
					null))
					.addParameter("q", String.format("%s:>%s", batchDescriptor.getBatchTokenPath(), batchDescriptor.getBatchToken()))
					.addParameter("sort", String.format("%s:asc", batchDescriptor.getBatchTokenPath()))
					.addParameter("size", String.valueOf(batchDescriptor.getBatchSize()))
					.build();

			log.trace("Retrieving batch {}", uri);
			HttpRequest batchRequest = HttpRequest.newBuilder()
					.GET()
					.uri(uri)
					.build();

			HttpResponse<String> response = performRequest(batchRequest, HttpResponse.BodyHandlers.ofString());

			SearchResponse searchResponse = objectMapper.readValue(response.body(), SearchResponse.class);

			List<IndexItem> batch = new ArrayList<>();
			SearchHits hits = searchResponse.getHits();

			int remaining = hits.getTotal();
			batchDescriptor.setRemaining(remaining);

			List<SearchHit> searchHits = hits.getHits();
			for (SearchHit hit : searchHits) {
				String id = hit.getId();

				log.trace("Creating index item for document {} {}", indexDescriptor.getSummary(), id);

				IndexItem indexItem = new IndexItem();
				indexItem.setId(id);
				indexItem.setIndex(indexDescriptor.getIndexName());
				indexItem.setSource(objectMapper.writeValueAsString(hit.getSource()));

				batchDescriptor.setBatchToken(hit.getSort().get(0));
				batch.add(indexItem);
			}

			return batch;
		} catch (IOException e) {
			throw new IndexRequestException(indexDescriptor, e);
		} catch (URISyntaxException e) {
			throw new ConfigurationException("Request URI not properly formed", indexDescriptor, e);
		}
	}

	@Override
	public Object max(String field, IndexDescriptor indexDescriptor) {
		try {
			URI uri = new URIBuilder(new URI(indexDescriptor.getProtocol(),
					null,
					indexDescriptor.getHost(),
					indexDescriptor.getPort(),
					"/" + indexDescriptor.getIndexName() + "/_search",
					null,
					null))
					.addParameter("q", "*:*")
					.addParameter("sort", String.format("%s:desc", field))
					.addParameter("size", "1")
					.build();

			HttpRequest maxRequest = HttpRequest.newBuilder()
					.GET()
					.uri(uri)
					.build();
			HttpResponse<String> response = performRequest(maxRequest, HttpResponse.BodyHandlers.ofString());

			SearchResponse searchResponse = objectMapper.readValue(response.body(), SearchResponse.class);
			return searchResponse.getHits().getHits().get(0).getSort().get(0);
		} catch (URISyntaxException | JsonProcessingException e) {
			throw new IndexRequestException("Failed to get max value of field", indexDescriptor, e);
		}
	}

	private <T> HttpResponse<T> performRequest(HttpRequest request, HttpResponse.BodyHandler<T> responseBodyHandler) throws IndexRequestException {
		HttpResponse<T> response;

		try {
			response = httpClient.send(request, responseBodyHandler);
		} catch (IOException | InterruptedException e) {
			throw new IndexRequestException("Request failed", e);
		}

		if (response.statusCode() != 200) {
			throw new IndexRequestException("Request responded with a non-200 status code", response);
		}

		return response;
	}


	@Override
	public boolean supportsIndex(String indexType) {
		return indexType.matches("(elasticsearch:5)(?:.+)?");
	}
}
