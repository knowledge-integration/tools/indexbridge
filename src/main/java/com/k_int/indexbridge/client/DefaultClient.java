/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.client;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.CountRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.elasticsearch.indices.ExistsRequest;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.error.BatchIndexingFailedException;
import com.k_int.indexbridge.error.IndexRequestException;
import com.k_int.indexbridge.error.MigrationException;
import com.k_int.indexbridge.error.MigrationFailureException;
import com.k_int.indexbridge.migration.IndexBatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
class DefaultClient implements TargetClient {

	private RestClient restClient;
	private ElasticsearchClient client;
	private ObjectMapper objectMapper;

	@Override
	public void ping(IndexDescriptor indexDescriptor) throws IndexRequestException {
		try {
			client.ping();
		} catch (IOException | ElasticsearchException e) {
			throw new IndexRequestException("Failed to ping index", indexDescriptor, e);
		}
	}

	@Override
	public boolean exists(IndexDescriptor indexDescriptor) {

		try {
			ExistsRequest existsRequest = new ExistsRequest.Builder()
					.index(indexDescriptor.getIndexName())
					.build();

			return client.indices().exists(existsRequest).value();
		} catch (IOException | ElasticsearchException e) {
			throw new IndexRequestException("Failed to check if index exists", indexDescriptor, e);
		}
	}

	@Override
	public int documentCount(IndexDescriptor indexDescriptor) {

		CountRequest countRequest = new CountRequest.Builder()
				.index(indexDescriptor.getIndexName())
				.build();

		try {
			return (int) client.count(countRequest).count();
		} catch (IOException | ElasticsearchException e) {
			throw new IndexRequestException("Failed to get document count", indexDescriptor, e);
		}
	}

	@Override
	public Object max(String field, IndexDescriptor indexDescriptor) {
		try {
			SearchResponse<Document> search = client.search(s -> s
							.index(indexDescriptor.getIndexName())
							.sort(srt -> srt.field(f -> f.field(field).order(SortOrder.Desc))),
					Document.class);

			List<Hit<Document>> hits = search.hits().hits();

			if (hits != null && !hits.isEmpty()) {
				return hits.get(0).sort().get(0);
			}
		} catch (IOException e) {
			throw new IndexRequestException("Failed to get max value of field", indexDescriptor, e);
		} catch (ElasticsearchException e) {
			log.warn("Failed to get max value of {} from {}", field, indexDescriptor.getSummary());
			return null;
		}

		return null;
	}

	/**
	 * <p>
	 * Bug in Elasticsearch Java Client means that settings.index.mapping.* are not serialised correctly,
	 * so we can't apply mapping files where we need to override defaults, such as
	 * </p>
	 * <code>settings.index.mapping.total_fields.limit</code>.
	 * <p>
	 * For now, we will use the rest client to create the index to avoid this.
	 * See
	 * <p>
	 * <ul>
	 *  <li><a href="https://discuss.elastic.co/t/missing-index-setting-in-the-new-java-client/294739">missing-index-setting-in-the-new-java-client/294739</a></li>
	 *  <li><a href="https://github.com/elastic/elasticsearch-specification/issues/1566">elasticsearch-specification/issues/1566</a></li>
	 * </ul>
	 */
	@Override
	public void createIndex(IndexDescriptor indexDescriptor) {

		Request request = new Request("PUT", indexDescriptor.getIndexName());

		log.debug("About to create index from descriptor {}", indexDescriptor.toString());
		if(indexDescriptor.getIndexConfigurationPath() != null && indexDescriptor.getIndexConfigurationPath().isEmpty())
			indexDescriptor.setIndexConfigurationPath(null);

		if (indexDescriptor.getIndexConfigurationPath() == null) {
			log.warn("Creating index {} without configuration file", indexDescriptor.getSummary());
		} else {
			log.trace("Creating index {} with index configuration {}",
					indexDescriptor.getSummary(),
					indexDescriptor.getIndexConfigurationPath()
			);
			try {
				log.trace("About to get index settings from {}", indexDescriptor.getIndexConfigurationPath());
				String index = new String(Files.readAllBytes(Paths.get(indexDescriptor.getIndexConfigurationPath())));
				request.setJsonEntity(index);
			} catch (IOException e) {
				throw new MigrationFailureException("Failed to read index settings", e);
			}

		}

		try {
			restClient.performRequest(request);
		} catch (IOException e) {
			throw new IndexRequestException("Failed to create index", indexDescriptor, e);
		}
	}

	@Override
	public void indexBatch(IndexDescriptor indexDescriptor, IndexBatch batch) throws IndexRequestException {

		if (batch.getIndexRequests().isEmpty()) {
			log.debug("Received an empty batch");
			return;
		}

		BulkRequest.Builder bulkRequestBuilder = new BulkRequest.Builder()
				.index(indexDescriptor.getIndexName());

		batch.getIndexRequests().forEach(indexRequest -> {
			log.trace("Adding item {} {} to index batch", indexDescriptor.getSummary(), indexRequest.getId());

			try {
				Document doc = objectMapper.readValue(indexRequest.getSource(), Document.class);

				bulkRequestBuilder.operations(o ->
						o.create(c -> c.id(indexRequest.getId())
								.index(indexRequest.getIndex())
								.document(doc)));
			} catch (JsonProcessingException e) {
				throw new MigrationFailureException("Failed to deserialise request document");
			}

		});

		try {
			log.trace("Performing bulk request");
			BulkResponse bulkResponse = client.bulk(bulkRequestBuilder.build());

			if (bulkResponse.errors()) {
				log.warn("Bulk index request has errors");
				bulkResponse.items().stream()
						.map(BulkResponseItem::error)
						.filter(Objects::nonNull).forEach(e -> log.debug(e.reason()));
			}

		} catch (IOException ex) {
			// IOExceptions can usually be retried
			throw new BatchIndexingFailedException(batch, indexDescriptor, this, ex);
		} catch (ElasticsearchException ex) {
			throw new MigrationException("Bulk index request failed", ex);
		}
	}

	@Override
	public void build(IndexDescriptor indexDescriptor) {
		RestClientBuilder restClientBuilder = RestClient.builder(
				new HttpHost(indexDescriptor.getHost(), indexDescriptor.getPort(), indexDescriptor.getProtocol())
		);

		if (indexDescriptor.getUsername() != null && indexDescriptor.getPassword() != null) {
			final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

			credentialsProvider.setCredentials(
					AuthScope.ANY,
					new UsernamePasswordCredentials(indexDescriptor.getUsername(), indexDescriptor.getPassword())
			);

			restClientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder
					.setDefaultCredentialsProvider(credentialsProvider));
		}

		RestClient restClient = restClientBuilder.build();

		ElasticsearchTransport transport = new RestClientTransport(
				restClient, new JacksonJsonpMapper()
		);

		this.objectMapper = new ObjectMapper();
		// Using the rest client in parallel with the java client
		this.restClient = restClient;
		this.client = new ElasticsearchClient(transport);
	}

	@Override
	public boolean supportsIndex(String indexType) {
		return indexType.matches("(elasticsearch:8)(?:.+)?");
	}
}
