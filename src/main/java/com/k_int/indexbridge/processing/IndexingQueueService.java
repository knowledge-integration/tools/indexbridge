/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.processing;

import com.k_int.indexbridge.client.TargetClient;
import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.migration.IndexBatch;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public interface IndexingQueueService {

	/**
	 * Queues a batch. Will block if the queue is full.
	 *
	 * @param batchFuture the batch to be queued
	 */
	void queueBatch(CompletableFuture<IndexBatch> batchFuture);

	/**
	 * Writes the batch to the filesystem, indexes it in bulk, then removes
	 * the batch from the filesystem
	 *
	 * @param batch The batch to be indexed
	 * @param indexDescriptor the index descriptor of the target index
	 * @param client the client that interacts with the target index
	 * @return a future indicating the result of the operation
	 */
	@Async
	CompletableFuture<IndexBatch> indexBatch(IndexBatch batch, IndexDescriptor indexDescriptor, TargetClient client);

	/**
	 * Scheduled method that checks the queue for completed or failed
	 * futures, to either be removed from the queue or re-queued for a retry.
	 */
	void handleQueue() throws ExecutionException, InterruptedException;

	int queueSize();

	/**
	 * Polls the queue until it is empty.
	 *
	 * @return a void future that completes after the indexing queue has cleared
	 */
	CompletableFuture<Void> queueCleared();

	/**
	 * Finds any persisted batches that may not have been indexed and re-queues them,
	 * returning a future that indicates that the queue has been cleared.
	 *
	 * @return a void future that completes after the indexing queue has cleared
	 */
	CompletableFuture<Void> requeueBatches(IndexDescriptor indexDescriptor, TargetClient client);
}
