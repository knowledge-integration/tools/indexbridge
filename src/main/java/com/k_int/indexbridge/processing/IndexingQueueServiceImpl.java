/**
 * 	IndexBridge
 *
 *  Copyright (C) 2022  Knowledge Integration
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.k_int.indexbridge.processing;

import com.k_int.indexbridge.config.IndexDescriptor;
import com.k_int.indexbridge.client.TargetClient;
import com.k_int.indexbridge.config.MigrationConfig;
import com.k_int.indexbridge.error.BatchIndexingFailedException;
import com.k_int.indexbridge.error.MigrationFailureException;
import com.k_int.indexbridge.migration.IndexBatch;
import com.k_int.indexbridge.persistence.BatchPersistenceManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

@Slf4j
@Service
class IndexingQueueServiceImpl implements IndexingQueueService {

	private final BatchPersistenceManager batchPersistenceManager;
	private final BlockingQueue<CompletableFuture<IndexBatch>> batchQueue;


	public IndexingQueueServiceImpl(BatchPersistenceManager batchPersistenceManager, MigrationConfig migrationConfig) {
		this.batchPersistenceManager = batchPersistenceManager;
		this.batchQueue = new LinkedBlockingQueue<>(migrationConfig.getQueueSize());
	}

	@Override
	public void queueBatch(CompletableFuture<IndexBatch> batchFuture) {
		try {
			batchQueue.put(batchFuture);
		} catch (InterruptedException e) {
			throw new MigrationFailureException("Adding to batch queue was interrupted", e);
		}
	}

	@Override
	public CompletableFuture<IndexBatch> indexBatch(IndexBatch batch, IndexDescriptor indexDescriptor, TargetClient client) {
		if (batch == null || batch.getBatchDescriptor() == null || batch.getIndexRequests().size() == 0) {
			log.debug("Received an empty batch");
			return CompletableFuture.completedFuture(batch);
		}

		try {
			batchPersistenceManager.write(batch);
			client.indexBatch(indexDescriptor, batch);
			batchPersistenceManager.delete(batch.getBatchDescriptor().getId());

			return CompletableFuture.completedFuture(batch);
		} catch (BatchIndexingFailedException ex) {
			return CompletableFuture.failedFuture(ex);
		}
	}

	@Override
	@Scheduled(fixedRate = 500)
	public void handleQueue() throws ExecutionException, InterruptedException {
		log.trace("Queue size {}", queueSize());

		if (batchQueue.isEmpty())
			return;

		CompletableFuture<IndexBatch> future = batchQueue.element();
		if (future.isCompletedExceptionally()) {
			try {
				future.get();
			} catch (ExecutionException | InterruptedException ex) {
				if (ex.getCause() instanceof BatchIndexingFailedException bif) {
					log.warn("Re-queueing failed batch {}", bif.getBatch().getBatchDescriptor().getId(), bif.getCause());
					batchQueue.remove();
					queueBatch(indexBatch(bif.getBatch(), bif.getIndexDescriptor(), bif.getClient()));
				} else throw ex;
			}
		} else if (future.isDone()) {
			batchQueue.remove();
		}
	}

	@Override
	public int queueSize() {
		return batchQueue.size();
	}

	@Override
	public CompletableFuture<Void> queueCleared() {

		return CompletableFuture.runAsync(() -> {
			do {
				log.trace("Waiting for queue to be cleared, current size is {}...", queueSize());
			} while (!batchQueue.isEmpty());
		});
	}

	@Override
	public CompletableFuture<Void> requeueBatches(IndexDescriptor indexDescriptor, TargetClient client) {
		String[] persistedBatches = batchPersistenceManager.getPersistedBatches();

		for (String batchId : persistedBatches) {
			IndexBatch batch = batchPersistenceManager.read(batchId);
			queueBatch(indexBatch(batch, indexDescriptor, client));
		}

		return queueCleared();
	}
}
