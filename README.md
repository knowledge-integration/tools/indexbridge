# IndexBridge

Supports index migration from `elasticsearch:5.*` to `elasticsearch:8.*`.

## Build and Deployment

### Dependencies

- `JDK >= 17`, or `JRE >= 17` if using a pre-packaged release

### Build

The application can be packaged for bare-metal deployment by executing the 
following gradle task:

Tar
```shell
./gradlew distTar
```
ZIP
```shell
./gradlew distZip
```

This will create a tar/zip archive in the `build/distributions` directory.
```shell
[indexbridge] ll build/distributions
-rw-rw-r--. 1 1000 1000 44646400 Mar 29 18:28 indexbridge-0.0.1-SNAPSHOT.tar
```
This can then be unpacked on the machine performing the migration. After
unpacking the directory should have the following structure:

```shell
indexbridge-0.0.1-SNAPSHOT
├── bin
│   ├── indexbridge
│   └── indexbridge.bat
├── config
│   ├── application.yml
│   └── log4j2.xml
└── lib
    ├── *.jar
```

## Usage

### Configuration

The migration configuration is stored in `config/application.yml`. Only values
that override their defaults need to be supplied.

| **Name**                        | **Path**                                                               | **Description**                                                                                                                                                                                                                                                                | **Default**       | **Mandatory** |
|---------------------------------|------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|---------------|
| Batch Size                      | `migration.batchSize`                                                  | The number of documents to retrieve from the source index, and to insert into the target index, in each request. This value has a direct impact on compute resource usage for both the migrator, source and target clusters.                                                   | `3000`            | &cross;       |
| Batch Token Initial Value       | `migration.batchTokenInitialValue`                                     | The value to start migration from. Should initially be set to the lowest possible value of `sourceIndexDescriptor.batchTokenPath`                                                                                                                                              | N/A               | &check;       |
| Allow Resume                    | `migration.allowResume`                                                | Whether to attempt to resume a previous migration in the case that the target index already contains documents.                                                                                                                                                                | `true`            | &cross;       |
| Queue Size                      | `migration.queueSize`                                                  | Maximum number of indexing batches that can be queued before the migration will block and wait for the next one to complete. If this is set too high, the migrator may exhaust the JVM heap and cause the application to terminate, in which case the value should be lowered. | `5`               | &cross;       |
| Queue Cleared Timeout           | `migration.queueClearedTimeout`                                        | Time in minutes to wait for the batch queue to complete before failing.                                                                                                                                                                                                        | `15`              | &cross;       |
| Source Index Name               | `migration.sourceIndexDescriptor.indexName`                            | The name of the index to be migrated. This index must exist for the migration to run.                                                                                                                                                                                          | `default`         | &cross;       |
| Source Cluster Name             | `migration.sourceIndexDescriptor.clusterName`                          | The name of the cluster containing the source index. Only used for logging.                                                                                                                                                                                                    | `elasticsearch`   | &cross;       |
| Source Cluster Hostname         | `migration.sourceIndexDescriptor.host`                                 | The hostname of the cluster containing the source index.                                                                                                                                                                                                                       | `localhost`       | &cross;       |
| Source Cluster Protocol         | `migration.sourceIndexDescriptor.protocol`                             | The protocol to use to connect to the source cluster. Supports `http` and `https`.                                                                                                                                                                                             | `http`            | &cross;       |
| Source Index Type               | `migration.sourceIndexDescriptor.type`                                 | The type and version of the source index. Used to locate applicable clients. Values must include the index distribution (elasticsearch/opensearch), followed by a colon and (at least) the index major version.                                                                | `elasticsearch:8` | &cross;       |
| Source Index Port               | `migration.sourceIndexDescriptor.port`                                 | The port to connect to the source cluster on.                                                                                                                                                                                                                                  | `9200`            | &cross;       |
| Source Batch Token Path         | `migration.sourceIndexDescriptor.batchTokenPath`                       | The path to the field used to paginate through the source index.                                                                                                                                                                                                               | `id`              | &cross;       |
| Source Cluster Username         | `migration.sourceIndexDescriptor.username`                             | **NOT IMPLEMENTED**                                                                                                                                                                                                                                                            | N/A               | &cross;       |
| Source Cluster Password         | `migration.sourceIndexDescriptor.password`                             | **NOT IMPLEMENTED**                                                                                                                                                                                                                                                            | N/A               | &cross;       |
| Target Index Name               | `migration.targetIndexDescriptor.indexName`                            | The name of the index to be created on the target cluster. **WARNING** If an index of the same name already exists, its contents may be overwritten or merged with the new data set.                                                                                           | `default`         | &cross;       |
| Target Cluster Name             | `migration.targetIndexDescriptor.clusterName`                          | The name of the cluster containing the target index. Only used for logging.                                                                                                                                                                                                    | `elasticsearch`   | &cross;       |
| Target Cluster Hostname         | `migration.targetIndexDescriptor.host`                                 | The hostname of the cluster containing the target index.                                                                                                                                                                                                                       | `localhost`       | &cross;       |
| Target Cluster Protocol         | `migration.targetIndexDescriptor.protocol`                             | The protocol to use to connect to the target cluster. Supports `http` and `https`.                                                                                                                                                                                             | `http`            | &cross;       |
| Target Index Type               | `migration.targetIndexDescriptor.type`                                 | The type and version of the target index. Used to locate applicable clients. Values must include the index distribution (elasticsearch/opensearch), followed by a colon and (at least) the index major version.                                                                | `elasticsearch:8` | &cross;       |
| Target Index Port               | `migration.targetIndexDescriptor.port`                                 | The port to connect to the target cluster on.                                                                                                                                                                                                                                  | `9200`            | &cross;       |
| Target Batch Token Path         | `migration.targetIndexDescriptor.batchTokenPath`                       | The path to the field used to get the resumption point from the target index, if `allowResume` is enabled.                                                                                                                                                                     | `id`              | &cross;       |
| Target Index Configuration Path | `migration.targetIndexDescriptor.indexConfigurationPath`               | The path to the file containing the index settings and mappings for the target index. Must be valid JSON. If not provided, the index will be created with the cluster's default settings.                                                                                      | N/A               | &cross;       |
| Target Cluster Username         | `migration.targetIndexDescriptor.username`                             | The username used to access the target cluster. If empty, authentication will not be attempted.                                                                                                                                                                                | N/A               | &cross;       |
| Target Cluster Password         | `migration.targetIndexDescriptor.password`                             | The password used to access the target cluster. If empty, authentication will not be attempted.                                                                                                                                                                                | N/A               | &cross;       |
| Maximum Retry Attempts          | `retry.maxAttempts`                                                    | The maximum number of retries to execute when an action fails. Retries are applied to all requests to the source and target index.                                                                                                                                             | `5`               | &cross;       |
| Initial Retry Interval          | `retry.initialInterval`                                                | The interval (in milliseconds) between the first failure and the first retry.                                                                                                                                                                                                  | `3000`            | &cross;       |
| Maximum Retry Interval          | `retry.maxInterval`                                                    | The interval (in milliseconds) between the retries.                                                                                                                                                                                                                            | `60000`           | &cross;       |
| Retry Interval Multiplier       | `retry.multiplier`                                                     | The multiplier to apply to  `initialInterval` after each failed action.                                                                                                                                                                                                        | `1.5`             | &cross;       |

### Running a Migration

After configuration, the migration can be started by executing the start script:

On UNIX systems:
```shell
./bin/indexbridge
```
On Windows systems:
```shell
./bin/indexbridge.bat
```
This will start IndexBridge and begin logging to `stdout`:

```
    ____          __             ____       _     __
   /  _/___  ____/ /__  _  __   / __ )_____(_)___/ /___ ____
   / // __ \/ __  / _ \| |/_/  / __  / ___/ / __  / __ `/ _ \
 _/ // / / / /_/ /  __/>  <   / /_/ / /  / / /_/ / /_/ /  __/
/___/_/ /_/\__,_/\___/_/|_|  /_____/_/  /_/\__,_/\__, /\___/
                                                /____/
Copyright (C) 2022 Knowledge Integration

2022-04-06 10:36:05,977 INFO Starting IndexBridge using Java 17 on kintlt with PID 556606
2022-04-06 10:36:05,979 INFO No active profile set, falling back to 1 default profile: "default"
2022-04-06 10:36:08,250 INFO Started IndexBridge in 2.777 seconds (JVM running for 4.38)
2022-04-06 10:36:08,251 INFO Preparing index migration...
2022-04-06 10:36:08,268 INFO Pinging source cluster source-cluster/source@localhost:9200...
2022-04-06 10:36:09,385 INFO Pinging target cluster target-cluster/target@localhost:9400...
2022-04-06 10:36:10,341 INFO Checking source index source-cluster/source@localhost:9200 exists...
2022-04-06 10:36:10,363 INFO Source index source-cluster/source@localhost:9200 contains 1000000 documents to be migrated
2022-04-06 10:36:10,365 INFO Index target-cluster/target@localhost:9400 does not exist, creating...
2022-04-06 10:36:10,370 INFO Checking for incomplete batches...
2022-04-06 10:36:14,395 INFO Starting migration from source-cluster/source@localhost:9200 to target-cluster/target@localhost:9400

```

After starting, IndexBridge will create the `logs` and `data` directories 
in the deployment directory. `logs` contains detailed migration logs, `data` contains 
queued batches, that will be removed after they have completed.
If batches exist in `data` when the migration is started, IndexBridge will attempt to 
complete them before starting the migration.

After the migration has completed, IndexBridge will report the execution time of the migration, as well
as the document count of the target index vs the source index (These may not match exactly until 
the target cluster itself has finished indexing).

The migration can be terminated at any time, and will attempt to resume from the same point 
(if `migration.allowResume` is set to `true`). 

## Issues or Feature Requests

Please [create an issue](https://gitlab.com/knowledge-integration/tools/indexbridge/-/issues/new) to request a new feature or 
report an issue. 

> Do not include private information in issues.

## License

Licensed under the GNU Affero General Public License. See `LICENSE.md` for full license.